package com.bennu.neo4Rec.enums;

import java.util.HashMap;

import org.springframework.lang.Nullable;

public enum ErrorCode {
	
	VALIDATION_ERROR(1, "Error during input validation"),
	INTERNAL_SERVER_ERROR(2, "You just broke me"),
	NOT_FOUND(3,"You sure that Id is correct?");
	
	private static final ErrorCode[] VALUES;

	static {
		VALUES = values();
	}

	private final int value;

	private final String reasonPhrase;

	ErrorCode(int value, String reasonPhrase) {
		this.value = value;
		this.reasonPhrase = reasonPhrase;
	}
	
	
	public HashMap<String,String> map(){
		HashMap<String,String> map = new HashMap<String,String>();
		map.put("value", Integer.toString(this.value));
		map.put("reasonPhrase", this.reasonPhrase);
		return map;
		
	}
	/**
	 * Return the integer value of this error code.
	 */
	public int value() {
		return this.value;
	}
	
	/**
	 * Return the reason phrase of this error code.
	 */
	public String getReasonPhrase() {
		return this.reasonPhrase;
	}
	
	public static ErrorCode valueOf(int statusCode) {
		ErrorCode status = resolve(statusCode);
		if (status == null) {
			throw new IllegalArgumentException("No matching constant for [" + statusCode + "]");
		}
		return status;
	}
	
	@Nullable
	public static ErrorCode resolve(int statusCode) {
		// Use cached VALUES instead of values() to prevent array allocation.
		for (ErrorCode status : VALUES) {
			if (status.value == statusCode) {
				return status;
			}
		}
		return null;
	}
}
