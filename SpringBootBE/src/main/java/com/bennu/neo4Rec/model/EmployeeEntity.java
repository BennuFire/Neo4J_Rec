package com.bennu.neo4Rec.model;


import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Node(labels = {"Employee"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeEntity extends Entity {

    @Property("emp_id")
    @JsonProperty("EMPLOYEE_ID")
    private int employee_id;

}
