package com.bennu.neo4Rec.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Property;
import lombok.Data;

@Data
public class Entity {
	
	@Id
	@GeneratedValue
    private Long id;

    @Property("name")
    private String name;
}
