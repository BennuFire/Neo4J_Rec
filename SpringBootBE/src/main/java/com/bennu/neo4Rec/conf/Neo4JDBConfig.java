package com.bennu.neo4Rec.conf;

import org.neo4j.driver.AccessMode;
import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.SessionConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.neo4j.config.AbstractReactiveNeo4jConfig;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class Neo4JDBConfig extends AbstractReactiveNeo4jConfig {

	@Bean
	public Driver driver() {

		Driver driver = GraphDatabase.driver("neo4j+s://781e9a7e.databases.neo4j.io:7687", AuthTokens.basic("neo4j", "YyO3ZT8_kyxeYqcaFTlLN43mXl9oFq6dVyIgaDO1ZW0"));
		SessionConfig sessionConfig = SessionConfig.builder().withDefaultAccessMode(AccessMode.READ).build();
		driver.rxSession(sessionConfig);
		return driver;
	}

}