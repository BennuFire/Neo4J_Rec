package com.bennu.neo4Rec.utilities;

import java.beans.FeatureDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class Util {

	private Util() {
	}
	
	public static Date fromLocalDatoToDate (LocalDateTime ldt) {
		return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
	}

	public static String[] getNullPropertyNames(Object source) {
		final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
		return Stream.of(wrappedSource.getPropertyDescriptors()).map(FeatureDescriptor::getName)
				.filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null).toArray(String[]::new);
	}

	public static <T> Object[] mergeArray(T[] arr1, T[] arr2) {
		return Stream.of(arr1, arr2).flatMap(Stream::of).toArray();
	}

	public static void copyProperties(Object src, Object target) {
		BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
	}

	public static void copyProperties(Object src, Object target, String[] extra) {
		Object[] o = mergeArray(getNullPropertyNames(src), extra);
		BeanUtils.copyProperties(src, target, Arrays.copyOf(o, o.length, String[].class));
	}

	public static Map<String, Object> buildPropertiesMapFromBean(Object object)
			throws IllegalAccessException, InvocationTargetException {
		Method[] methods = object.getClass().getMethods();

		Map<String, Object> result = new HashMap<>();
		for (Method method : methods) {
			if (isValidGetter(method)) {
				result.put(toSnakeCase(method.getName().substring(3)), method.invoke(object));
			}
		}

		return result;
	}

	public static String entityToLabel(String entity) {
		CharSequence firstChar = entity.subSequence(0, 1);
		CharSequence remaining = entity.subSequence(1, entity.length());
		return firstChar.toString().concat(remaining.toString().toLowerCase());
	}

	private static boolean isValidGetter(Method method) {
		return (method.getName().startsWith("get") && method.getParameterCount() == 0
				&& !(void.class.equals(method.getReturnType())) && !method.getName().equals("getClass")
				&& !method.getName().equals("getDictionary"));
	}

	private static String toSnakeCase(String string) {
		Pattern camelCaseSpacing = Pattern.compile("(?<=.)(?=[A-Z][a-z])");
		String[] stringWords = camelCaseSpacing.split(string);

		return String.join("_", stringWords).toUpperCase();
	}

}
