package com.bennu.neo4Rec.utilities;

public class Constants {

	private Constants() {
	}

	public static final String CREATE_BASE_URL = "/create/{entity}";
	public static final String FIND_BY_ID_BASE_URL = "/findById/{entity}/{ID}";
	public static final String UPDATE_ID_BASE_URL = "/update/{entity}/{ID}";
	public static final String DELETE_BY_ID_BASE_URL = "/delete/{entity}/{ID}";
	public static final String GET_ALL_STREAMING_BASE_URL = "/findS/{entity}";
	public static final String GET_ALL_BASE_URL = "/find/{entity}";
	
	public static final String ID = "ID";
	public static final String NAME = "NAME";
	public static final String FIELD_CANNOT_BE_NULL_OR_EMPTY = "Field cannot be null or empty";
	
}
