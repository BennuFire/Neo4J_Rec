package com.bennu.neo4Rec.validator;

import java.util.List;

import com.bennu.neo4Rec.controller.model.ValidationError;

public interface FilterRequestValidator<T> {

/**
* Validate incoming HTTP request body or part of it
* @param requestBody
* @return List of Validation Errors if validation rules do not apply
*/
public List<ValidationError> validate(T requestBody);
}