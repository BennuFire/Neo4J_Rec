package com.bennu.neo4Rec.validator;

import static com.bennu.neo4Rec.utilities.Constants.ID;
import static com.bennu.neo4Rec.utilities.Constants.FIELD_CANNOT_BE_NULL_OR_EMPTY;
import static com.bennu.neo4Rec.utilities.Constants.NAME;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.bennu.neo4Rec.controller.model.ValidationError;
import com.bennu.neo4Rec.controller.model.dto.Dto;


public class CommonBodyValidator<T extends Dto> implements FilterRequestValidator<T> {

	@Override
	public List<ValidationError> validate(T requestBody) {
		List<ValidationError> errors = new ArrayList<>();
		if (isNullOrEmpty(requestBody.getName())) {
			errors.add(new ValidationError(NAME, FIELD_CANNOT_BE_NULL_OR_EMPTY));
		}
		/*if (isNullOrEmpty(requestBody.getInternal())) {
			errors.add(new ValidationError(ID, FIELD_CANNOT_BE_NULL_OR_EMPTY));
		}*/
		return errors;
		
	}

	private boolean isNullOrEmpty(String field) {
		return Objects.isNull(field) || field.isBlank();
	}

}
