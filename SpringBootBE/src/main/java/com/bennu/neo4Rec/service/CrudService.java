package com.bennu.neo4Rec.service;

import java.util.List;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CrudService<T> {

	Mono<T> create(T dto);

	Mono<T> findById(Long id);

	Mono<T> updateById(Long id, T dto);

	Mono<String> deleteById(Long id);
	
	Flux<T> getAllS();
	
	Mono<List<T>> getAll();

}
