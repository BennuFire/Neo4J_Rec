package com.bennu.neo4Rec.service.exception;

public class EntityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 9043982849172574387L;

	public EntityNotFoundException(String entity, String id) {
		super(new StringBuilder().append("No ").append(entity).append(" found with ID: ").append(id).toString());
	}

}
