package com.bennu.neo4Rec.service.impl;

import static com.bennu.neo4Rec.utilities.EntityEnums.EMPLOYEE;

import java.time.Duration;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bennu.neo4Rec.controller.model.dto.EmployeeDto;
import com.bennu.neo4Rec.mapper.EmployeeMapper;
import com.bennu.neo4Rec.model.EmployeeEntity;
import com.bennu.neo4Rec.repository.EmployeeRepository;
import com.bennu.neo4Rec.service.CrudService;
import com.bennu.neo4Rec.utilities.Util;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service("employeeService")
public class EmployeeService extends CommonService<EmployeeEntity, EmployeeDto> implements CrudService<EmployeeDto> {

	@Autowired
	private EmployeeMapper mapper;

	public EmployeeService(EmployeeRepository employeeRepository) {
		super(employeeRepository, EMPLOYEE);
	}

	@Override
	@Transactional(value = "reactiveTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Mono<EmployeeDto> create(EmployeeDto employeeDto) {
		return createEntity(mapper.toEntity(employeeDto)).map(mapper::toDto);
	}

	@Override
	@Transactional(value = "reactiveTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Mono<EmployeeDto> findById(Long id) {
		return findEntityById(id).map(mapper::toDto);
	}

	@Override
	@Transactional(value = "reactiveTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Mono<EmployeeDto> updateById(Long id, EmployeeDto employeeDto) {
		return updateEntity(employeeDto, id).map(mapper::toDto);

	}

	@Override
	@Transactional(value = "reactiveTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Mono<String> deleteById(Long id) {
		return deleteEntityById(id);
	}
	
	@Override
	@Transactional(value = "reactiveTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Flux<EmployeeDto> getAllS() {
		return Flux.zip(getAllEntity().map(mapper::toDto) , Flux.interval(Duration.ofMillis(50)), 
				(key,value) -> key);
	}
	
	@Override
	@Transactional(value = "reactiveTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Mono<List<EmployeeDto>> getAll() {
		return getAllEntity().map(mapper::toDto).collectList();
	}
}
