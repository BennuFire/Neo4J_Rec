package com.bennu.neo4Rec.service.exception;

public class ServiceValidationException extends RuntimeException{

	private static final long serialVersionUID = 6519328369426244627L;
	
	public ServiceValidationException(String error) {
		super(error);
	}

}
