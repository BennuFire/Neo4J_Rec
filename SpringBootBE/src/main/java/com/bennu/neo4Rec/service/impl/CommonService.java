package com.bennu.neo4Rec.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bennu.neo4Rec.controller.model.dto.Dto;
import com.bennu.neo4Rec.model.Entity;
import com.bennu.neo4Rec.repository.EntityRepository;
import com.bennu.neo4Rec.service.exception.EntityNotFoundException;
import com.bennu.neo4Rec.utilities.EntityEnums;
import com.bennu.neo4Rec.utilities.Util;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

class CommonService<E extends Entity, D extends Dto> {


	private EntityRepository<E> entityRepository;
	private EntityEnums entity;

	protected CommonService(EntityRepository<E> entityRepository, EntityEnums entity) {
		this.entityRepository = entityRepository;
		this.entity = entity;
	}

	@Transactional(value = "reactiveTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Mono<E> createEntity(E e) {
		return entityRepository.save(e);
	}

	@Transactional(value = "reactiveTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Mono<E> findEntityById(Long id) {
		return entityRepository.findById(id);
	}

	@Transactional(value = "reactiveTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Mono<E> updateEntity(D dto, Long id) {
		return entityRepository.findById(id).flatMap(e -> {
					String[] propertiesToIgnore = {};
					Util.copyProperties(dto, e, propertiesToIgnore);
					return entityRepository.save(e);
				});
	}

	@Transactional(value = "reactiveTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Mono<String> deleteEntityById(Long id) {
		return entityRepository.deleteById(id).then(Mono.just("Deleted")); 
	}
	
	@Transactional(value = "reactiveTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Flux<E> getAllEntity() {
		return entityRepository.findAll(); 
	}
}
