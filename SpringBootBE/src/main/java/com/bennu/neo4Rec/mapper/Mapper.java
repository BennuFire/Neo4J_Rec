package com.bennu.neo4Rec.mapper;

public interface Mapper<D , P> {
    /**
     * Convert an Object to a DTO.
     *
     * @param entity to convert
     * @return the DTO
     */
    D toDto(P projection);
}
