package com.bennu.neo4Rec.mapper;


import com.bennu.neo4Rec.controller.model.dto.EmployeeDto;
import com.bennu.neo4Rec.model.EmployeeEntity;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeMapper implements Mapper<EmployeeDto, EmployeeEntity> {
    
    public EmployeeEntity toEntity(EmployeeDto dto) {
    	EmployeeEntity entity = new EmployeeEntity();
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }

    
    public EmployeeDto toDto(EmployeeEntity entity) {
        return EmployeeDto.builder().ID(entity.getEmployee_id())
                .name(entity.getName())
                .internal(entity.getId())
                .build();
    }
}
