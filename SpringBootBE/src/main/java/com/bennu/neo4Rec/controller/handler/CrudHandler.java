package com.bennu.neo4Rec.controller.handler;

import static com.bennu.neo4Rec.utilities.Constants.ID;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.bennu.neo4Rec.controller.handler.CrudHandler;
import com.bennu.neo4Rec.service.CrudService;
import com.bennu.neo4Rec.service.exception.EntityNotFoundException;
import com.bennu.neo4Rec.validator.FilterRequestValidator;
import com.bennu.neo4Rec.controller.model.ValidationError;

import reactor.core.publisher.Mono;

public abstract class CrudHandler<T, S extends CrudService<T>> extends CommonHandler {
	private static Logger logger = LoggerFactory.getLogger(CrudHandler.class.getName());
	private final Class<T> validationClass;
	private final S service;
	private final FilterRequestValidator<T> validator;

	protected CrudHandler(S service, FilterRequestValidator<T> validator, Class<T> clazz) {
		this.service = service;
		this.validator = validator;
		this.validationClass = clazz;
	}

	protected Mono<ServerResponse> handleCreate(final ServerRequest request) {
		return request.bodyToMono(validationClass).flatMap(body -> {
			List<ValidationError> errors = validator.validate(body);
			if (errors.isEmpty()) {
				return processCreate(body);
			} else {
				return onValidationErrors(errors);
			}
		}).onErrorResume(Exception.class, this::onGenericError);
	}

	protected Mono<ServerResponse> handleUpdate(final ServerRequest request) {
		String idString = request.pathVariable(ID);
		Long idLong = Long.parseLong(idString);
		return request.bodyToMono(validationClass).flatMap(body -> {
			List<ValidationError> errors = validator.validate(body);

			if (errors.isEmpty()) {
				return processUpdate(body, idLong);
			} else {
				return onValidationErrors(errors);
			}
		}).onErrorResume(Exception.class, this::onGenericError);
	}

	protected Mono<ServerResponse> handleFindById(ServerRequest request) {
		String idString = request.pathVariable(ID);
		Long idLong = Long.valueOf(idString);
		return service.findById(idLong).flatMap(this::onResponseOk)
				.onErrorResume(EntityNotFoundException.class, this::onEntityNotFoundError)
				.onErrorResume(Exception.class, this::onGenericError);
	}

	protected Mono<ServerResponse> handleDeleteById(ServerRequest request) {
		String idString = request.pathVariable(ID);
		Long idLong = Long.parseLong(idString);
		return service.deleteById(idLong).flatMap(this::onResponseOk)
				.onErrorResume(EntityNotFoundException.class, this::onEntityNotFoundError)
				.onErrorResume(Exception.class, this::onGenericError);
	}

	protected Mono<ServerResponse> handleGetAllS(ServerRequest request) {
		return ServerResponse.status(HttpStatus.OK).contentType(MediaType.TEXT_EVENT_STREAM)
				.body(service.getAllS(), this.validationClass);
	}
	
	protected Mono<ServerResponse> handleGetAll(ServerRequest request) {
		return ServerResponse.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
				.body(service.getAll(), this.validationClass);
	}

	private Mono<ServerResponse> processCreate(T body) {
		return service.create(body).flatMap(this::onResponseOk).onErrorResume(Exception.class,
				this::onGenericError);
	}
	
	private Mono<ServerResponse> processUpdate(T body, Long id) {
		return service.updateById(id, body).flatMap(this::onResponseOk)
				.onErrorResume(EntityNotFoundException.class, this::onEntityNotFoundError)
				.onErrorResume(Exception.class, this::onGenericError);
	}

}

