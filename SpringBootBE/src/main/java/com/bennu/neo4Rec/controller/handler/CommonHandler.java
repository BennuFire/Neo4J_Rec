package com.bennu.neo4Rec.controller.handler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.bennu.neo4Rec.controller.model.RestResponse;
import com.bennu.neo4Rec.controller.model.ValidationError;
import com.bennu.neo4Rec.enums.ErrorCode;
import com.bennu.neo4Rec.service.exception.EntityNotFoundException;
import com.bennu.neo4Rec.service.exception.ServiceValidationException;


import reactor.core.publisher.Mono;

public abstract class CommonHandler {

	private static Logger logger = LoggerFactory.getLogger(CommonHandler.class.getName());

	protected Mono<ServerResponse> onResponseOk(Object element) {
		logger.info("OUTPUT: {}", element);
		return ServerResponse.status(HttpStatus.OK).bodyValue(new RestResponse<>(element));
	}

	protected Mono<ServerResponse> onGenericError(Throwable e) {
		String errorMessage = logError(e);
		return ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.bodyValue(new RestResponse<>(ErrorCode.INTERNAL_SERVER_ERROR, errorMessage));
	}

	protected Mono<ServerResponse> onServiceValidationError(ServiceValidationException e) {
		logError(e);
		return Mono.just(e.getMessage()).flatMap(s -> ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.bodyValue(new RestResponse<>(ErrorCode.VALIDATION_ERROR, s)));
	}
	
	protected Mono<ServerResponse> onEntityNotFoundError(EntityNotFoundException e) {
		String errorMessage = logError(e);
		return ServerResponse.status(HttpStatus.OK).bodyValue(new RestResponse<>(ErrorCode.NOT_FOUND, errorMessage));
	}
	
	protected Mono<ServerResponse> onValidationErrors(List<ValidationError> errors) {
		String validationErrorString = errors.toString();
		logger.error("SOME ERROR OCCURRED: {}", validationErrorString);

		return ServerResponse.status(HttpStatus.BAD_REQUEST).bodyValue(new RestResponse<>(errors));
	}


	private String logError(Throwable e) {
		String errorMessage = e.getMessage();
		logger.error("SOME ERROR OCCURRED: {}", errorMessage);
		return errorMessage;
	}

}
