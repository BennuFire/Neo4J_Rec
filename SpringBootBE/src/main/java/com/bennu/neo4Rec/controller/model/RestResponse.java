package com.bennu.neo4Rec.controller.model;

import java.io.Serializable;
import java.util.EnumMap;

import com.bennu.neo4Rec.enums.ErrorCode;

import lombok.Getter;

@Getter
public class RestResponse<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private EnumMap<ErrorCode, String> errorMessages;
	private transient T output;
	
	public RestResponse(T output) {
		super();
		this.output = output;
	}
	
	public RestResponse(EnumMap<ErrorCode,String> errorMessages, T  output) {
		super();
		this.errorMessages = errorMessages;
		this.output = output;
	}
	
	public RestResponse(ErrorCode errorCode, String errorMessage) {
		super();
		this.errorMessages = new EnumMap<>(ErrorCode.class);
		errorMessages.put(errorCode, errorMessage);
	}
	

}