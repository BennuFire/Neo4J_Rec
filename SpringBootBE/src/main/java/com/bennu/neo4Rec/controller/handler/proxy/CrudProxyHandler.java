package com.bennu.neo4Rec.controller.handler.proxy;

import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

public interface CrudProxyHandler {
	public Mono<ServerResponse> handleFindById(ServerRequest request);

	public Mono<ServerResponse> handleDeleteById(ServerRequest request);

	public Mono<ServerResponse> handleCreate(ServerRequest request);

	public Mono<ServerResponse> handleUpdate(ServerRequest request);

	public Mono<ServerResponse> handleGetAllS(ServerRequest request);
	
	public Mono<ServerResponse> handleGetAll(ServerRequest request);

}
