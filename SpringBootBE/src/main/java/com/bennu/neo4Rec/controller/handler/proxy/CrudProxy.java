package com.bennu.neo4Rec.controller.handler.proxy;

import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.bennu.neo4Rec.controller.handler.CrudHandler;
import com.bennu.neo4Rec.service.CrudService;
import com.bennu.neo4Rec.validator.FilterRequestValidator;

import reactor.core.publisher.Mono;

public class CrudProxy<T> extends CrudHandler<T, CrudService<T>> implements CrudProxyHandler {

	public CrudProxy(CrudService<T> service, Class<T> validationClazz, FilterRequestValidator<T> validator) {
		super(service, validator, validationClazz);
	}

	@Override
	public Mono<ServerResponse> handleCreate(ServerRequest request) {
		return super.handleCreate(request);
	}

	@Override
	public Mono<ServerResponse> handleUpdate(ServerRequest request) {
		return super.handleUpdate(request);
	}

	@Override
	public Mono<ServerResponse> handleFindById(ServerRequest request) {
		return super.handleFindById(request);
	}

	@Override
	public Mono<ServerResponse> handleDeleteById(ServerRequest request) {
		return super.handleDeleteById(request);
	}

	@Override
	public Mono<ServerResponse> handleGetAllS(ServerRequest request) {
		return super.handleGetAllS(request);
	}
	
	@Override
	public Mono<ServerResponse> handleGetAll(ServerRequest request) {
		return super.handleGetAll(request);
	}
	
	

}
