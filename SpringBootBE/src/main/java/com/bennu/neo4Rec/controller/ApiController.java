package com.bennu.neo4Rec.controller;

import static com.bennu.neo4Rec.utilities.Constants.CREATE_BASE_URL;
import static com.bennu.neo4Rec.utilities.Constants.GET_ALL_STREAMING_BASE_URL;
import static com.bennu.neo4Rec.utilities.Constants.DELETE_BY_ID_BASE_URL;
import static com.bennu.neo4Rec.utilities.Constants.FIND_BY_ID_BASE_URL;
import static com.bennu.neo4Rec.utilities.Constants.UPDATE_ID_BASE_URL;
import static com.bennu.neo4Rec.utilities.Constants.GET_ALL_BASE_URL;
import static org.springframework.web.reactive.function.server.RequestPredicates.DELETE;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.PUT;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import org.springdoc.core.annotations.RouterOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.bennu.neo4Rec.controller.model.RestResponse;
import com.bennu.neo4Rec.controller.model.dto.Dto;
import com.bennu.neo4Rec.utilities.EntityEnums;
import com.bennu.neo4Rec.controller.handler.proxy.CrudProxyFactory;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Controller("apiController")
public class ApiController {

	@Autowired
	private CrudProxyFactory proxyFactory;
	
	
	@Bean("createEndpoint")
	@Order(0)
	@RouterOperation(operation = @Operation(operationId = "create", summary = "Adding Entity", tags = {
			"CRUD" }, requestBody = @RequestBody(content = @Content(schema = @Schema(oneOf = Dto.class))), responses = {
							@ApiResponse(responseCode = "201", description = "Successful Operation", content = {
									@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.ALL_VALUE),
									@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_JSON_VALUE),
									@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_XML_VALUE) }),
							@ApiResponse(responseCode = "405", description = "Method temporary not allowed"),
							@ApiResponse(responseCode = "500", description = "Internal Server Error") }))
	public RouterFunction<ServerResponse> create() {
		return route(POST(CREATE_BASE_URL), proxyFactory::handleCreate);
	}
	
		@Bean("findByIdEndpoint")
		@Order(1)
		@RouterOperation(operation = @Operation(operationId = "findById", summary = "Find by ID entity", tags = {
				"CRUD" }, parameters = {
						@Parameter(in = ParameterIn.PATH, name = "entity", description = "Entity type", required = true, schema = @Schema(implementation = EntityEnums.class)),
						@Parameter(in = ParameterIn.PATH, name = "ID", description = "ID to find", required = true) }, responses = {
								@ApiResponse(responseCode = "200", description = "Successful Operation", content = {
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.ALL_VALUE),
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_JSON_VALUE),
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_XML_VALUE) }),
								@ApiResponse(responseCode = "405", description = "Method temporary not allowed"),
								@ApiResponse(responseCode = "500", description = "Internal Server Error") }))
		public RouterFunction<ServerResponse> findById() {
			return route(GET(FIND_BY_ID_BASE_URL), proxyFactory::handleFindById);
		}

		@Bean("updateEndpoint")
		@Order(2)
		@RouterOperation(operation = @Operation(operationId = "update", summary = "Update entity by ID", tags = {
				"CRUD" }, requestBody = @RequestBody(content = @Content(schema = @Schema(oneOf = Dto.class))), parameters = {
						@Parameter(in = ParameterIn.PATH, name = "entity", description = "Entity type", required = true, schema = @Schema(implementation = EntityEnums.class)),
						@Parameter(in = ParameterIn.PATH, name = "ID", description = "Entity ID to update", required = true)}, responses = {
								@ApiResponse(responseCode = "200", description = "Successful Operation", content = {
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.ALL_VALUE),
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_JSON_VALUE),
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_XML_VALUE) }),
								@ApiResponse(responseCode = "405", description = "Method temporary not allowed"),
								@ApiResponse(responseCode = "500", description = "Internal Server Error") }))
		public RouterFunction<ServerResponse> updateById() {
			return route(PUT(UPDATE_ID_BASE_URL), proxyFactory::handleUpdate);
		}

		@Bean("deleteByIdEndpoint")
		@Order(3)
		@RouterOperation(operation = @Operation(operationId = "deleteById", summary = "Delete by ID", tags = {
				"CRUD" }, parameters = {
						@Parameter(in = ParameterIn.PATH, name = "entity", description = "Entity type", required = true, schema = @Schema(implementation = EntityEnums.class)),
						@Parameter(in = ParameterIn.PATH, name = "ID", description = "Entity ID to delete", required = true)}, responses = {
								@ApiResponse(responseCode = "200", description = "Successful Operation", content = {
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.ALL_VALUE),
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_JSON_VALUE),
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_XML_VALUE) }),
								@ApiResponse(responseCode = "405", description = "Method temporary not allowed"),
								@ApiResponse(responseCode = "500", description = "Internal Server Error") }))
		public RouterFunction<ServerResponse> deleteById() {
			return route(DELETE(DELETE_BY_ID_BASE_URL), proxyFactory::handleDeleteById);
		}
		
		@Bean("getAllSEndPoint")
		@Order(4)
		@RouterOperation(operation = @Operation(operationId = "create", summary = "Adding Employee", tags = {
				"CRUD" }, parameters = { @Parameter(in = ParameterIn.PATH, name = "entity", description = "Entity type", required = true, schema = @Schema(implementation = EntityEnums.class))}
						, responses = {
								@ApiResponse(responseCode = "201", description = "Successful Operation", content = {
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.ALL_VALUE),
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_JSON_VALUE),
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_XML_VALUE) }),
								@ApiResponse(responseCode = "405", description = "Method temporary not allowed"),
								@ApiResponse(responseCode = "500", description = "Internal Server Error") }))
		public RouterFunction<ServerResponse> getAllS() {
			return route(GET(GET_ALL_STREAMING_BASE_URL), proxyFactory::handleGetAllS);
		}
		
		@Bean("getAllEndPoint")
		@Order(4)
		@RouterOperation(operation = @Operation(operationId = "create", summary = "Adding Employee", tags = {
				"CRUD" }, parameters = { @Parameter(in = ParameterIn.PATH, name = "entity", description = "Entity type", required = true, schema = @Schema(implementation = EntityEnums.class))}
						, responses = {
								@ApiResponse(responseCode = "201", description = "Successful Operation", content = {
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.ALL_VALUE),
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_JSON_VALUE),
										@Content(schema = @Schema(implementation = RestResponse.class), mediaType = MediaType.APPLICATION_XML_VALUE) }),
								@ApiResponse(responseCode = "405", description = "Method temporary not allowed"),
								@ApiResponse(responseCode = "500", description = "Internal Server Error") }))
		public RouterFunction<ServerResponse> getAll() {
			return route(GET(GET_ALL_BASE_URL), proxyFactory::handleGetAll);
		}
	
}
