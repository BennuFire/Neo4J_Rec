package com.bennu.neo4Rec.controller.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidationError {

    @JsonProperty("field")
    private String field;

    @JsonProperty("error")
    private String error;
}