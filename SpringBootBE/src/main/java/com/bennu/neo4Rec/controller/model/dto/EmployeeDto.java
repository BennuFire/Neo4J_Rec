package com.bennu.neo4Rec.controller.model.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@SuperBuilder
@XmlRootElement
public class EmployeeDto extends Dto {

	@JsonProperty("employee_id")
	private int ID;
}
