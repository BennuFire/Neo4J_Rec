package com.bennu.neo4Rec.controller.handler.proxy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.bennu.neo4Rec.controller.model.dto.EmployeeDto;
import com.bennu.neo4Rec.service.impl.EmployeeService;
import com.bennu.neo4Rec.validator.CommonBodyValidator;

import reactor.core.publisher.Mono;

@Component
public class CrudProxyFactory {

	private CrudProxyHandler handler;
	@Autowired
	private EmployeeService employeeService;
	

	public Mono<ServerResponse> handleCreate(ServerRequest request) {
		setUp(request);
		return handler.handleCreate(request);
	}

	public Mono<ServerResponse> handleFindById(ServerRequest request) {
		setUp(request);
		return handler.handleFindById(request);
	}

	public Mono<ServerResponse> handleUpdate(ServerRequest request) {
		setUp(request);
		return handler.handleUpdate(request);
	}

	public Mono<ServerResponse> handleDeleteById(ServerRequest request) {
		setUp(request);
		return handler.handleDeleteById(request);
	}

	public Mono<ServerResponse> handleGetAllS(ServerRequest request) {
		setUp(request);
		return handler.handleGetAllS(request);
	}
	
	public Mono<ServerResponse> handleGetAll(ServerRequest request) {
		setUp(request);
		return handler.handleGetAll(request);
	}

	private void setUp(ServerRequest request) {
		//Here you can proxy crud for other entities
		if (true) {
			this.handler = new CrudProxy<>(employeeService, EmployeeDto.class, new CommonBodyValidator<>());

		}
	}

}
