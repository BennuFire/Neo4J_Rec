package com.bennu.neo4Rec.repository;

import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository;
import org.springframework.data.neo4j.repository.support.ReactiveCypherdslStatementExecutor;
import org.springframework.stereotype.Repository;

import com.bennu.neo4Rec.model.Entity;

@Repository
public interface EntityRepository<E extends Entity>
		extends ReactiveNeo4jRepository<E, Long>, ReactiveCypherdslStatementExecutor<E> {

}
