package com.bennu.neo4Rec.repository;

import org.springframework.stereotype.Repository;
import com.bennu.neo4Rec.model.EmployeeEntity;

@Repository("employeeRepository")
public interface EmployeeRepository extends EntityRepository<EmployeeEntity> {

}
