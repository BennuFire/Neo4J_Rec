package com.bennu.neo4Rec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching(proxyTargetClass = true)
public class Neo4RecApplication {

	public static void main(String[] args) {
		SpringApplication.run(Neo4RecApplication.class, args);
	}

}
