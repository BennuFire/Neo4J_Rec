//I was not sure about if Jacob team was going to include people directed by him fistline of employees but surprise!
//They don't exist.
MATCH (boss:Employee { name : 'Jacob'})
CALL apoc.path.subgraphNodes(boss, {
	relationshipFilter: "<HAS_BOSS",
    minLevel: 1,
    maxLevel: 2
})
YIELD node
with node where not (node)-[:IS_FRIEND_WITH]-(boss)
RETURN node;