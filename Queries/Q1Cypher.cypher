//We may not need :Person on MATCH(employees:Person) in order to avoid an extra filter step
MATCH(employees:Employee)-[:HAS_BOSS*]->(boss:Employee { name : 'Darth Vader'})
return employees