MATCH (boss:Employee { name : 'Darth Vader'})
CALL apoc.path.subgraphNodes(boss, {
	relationshipFilter: "<HAS_BOSS",
    minLevel: 1
})
YIELD node
RETURN node;