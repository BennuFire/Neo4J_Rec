//Token will change from one import to another

:param uriFile1 => 'https://raw.githubusercontent.com/BennuFire/Neo4J_Rec/main/DevOps/Database/import/employees1.csv?token=ABYOAI6AZ5EM3I5NFHGMQRDBINOWK';
:param uriFile2 => 'https://raw.githubusercontent.com/BennuFire/Neo4J_Rec/main/DevOps/Database/import/employees2.csv?token=ABYOAI6SOEOYAOPSQFZTDS3BINO42';


MATCH (n) detach delete n;

LOAD CSV WITH HEADERS FROM $uriFile1 AS row
MERGE(employee:Employee {name : row['employee name']})
MERGE(boss:Employee {name : row['has boss']})
CREATE(employee)-[:HAS_BOSS]->(boss)
return row;

LOAD CSV WITH HEADERS FROM $uriFile2 AS row
MERGE(p1:Employee {name : row['employee name']})
MERGE(p2:Employee {name : row['is friends with']})
CREATE(p1)-[:IS_FRIEND_WITH]->(p2)
return row;

CREATE INDEX name_person for (n:Employee) on (n.name);
CREATE INDEX name_employee for (n:Employee) on (n.name);

// MATCH(n:Person) remove n:Person set n:Employee