# Neo4J_Rec

This project is part of the Neo4J Consultant Engineer recruitment process. I'm just tryin' to do my best, have fun, and showcase a bit of me.
I hope my comments all around may guide you here, in case it's not enough, just try with the soundtrack on.


Soundtrack:

https://www.youtube.com/watch?v=SW6L_lTrIFg&ab_channel=NPRMusic

https://www.youtube.com/watch?v=QrR_gm6RqCo&ab_channel=NPRMusic

https://www.youtube.com/watch?v=F4neLJQC1_E&ab_channel=NPRMusic

https://www.youtube.com/watch?v=-Q9uIhuvUKc&ab_channel=NPRMusic

https://www.youtube.com/watch?v=GVDJ8O3lPBA&t=3s&ab_channel=NPRMusic

https://www.youtube.com/watch?v=hxsJvKYyVyg&ab_channel=NPRMusic


Swagger BE: http://bennuexecutor.3utilities.com:8080/webjars/swagger-ui/index.html?configUrl=/api-docs/swagger-config#/

TODO:
    1. Add emp_ids to migrated employees
    2. Check create with new config
    3. Tests!
    4. Neo4J on EC2?
    5. FE! Maybe Graphql stack
    6. ~~Solve streaming on EC2~~
    7. CD/CI
